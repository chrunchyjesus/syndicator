CREATE TRIGGER IF NOT EXISTS article_count_delete_total_and_unread AFTER DELETE ON articles
BEGIN
	DELETE FROM article_count_cache WHERE url = OLD.subscription_data_id;
	INSERT OR REPLACE INTO article_count_cache (url, total, unread)
		SELECT a1.subscription_data_id AS url, total, CASE WHEN unread IS NULL THEN 0 ELSE unread END unread
		FROM
			(SELECT subscription_data_id, count(*) AS total FROM articles WHERE subscription_data_id = OLD.subscription_data_id GROUP BY subscription_data_id) a1
		LEFT join
			(SELECT subscription_data_id, count(*) AS unread FROM articles WHERE subscription_data_id = OLD.subscription_data_id AND is_read = 0 GROUP BY subscription_data_id) a2
		ON a1.subscription_data_id = a2.subscription_data_id;
END;
