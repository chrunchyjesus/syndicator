create table subscriptions_tmp (
    url text not null primary key,
    subscription_order integer not null unique,
    proxy text
);

INSERT INTO subscriptions_tmp (url, subscription_order, proxy)
       SELECT url, subscription_order, proxy FROM subscriptions;
DROP TABLE subscriptions;
ALTER TABLE subscriptions_tmp RENAME TO subscriptions;
