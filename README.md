# Syndicator

> A terminal feedreader

Syndicator is a terminal feedreader currently in alpha.

## Getting Started

### Prerequsites

Things you need to build and run this project:

- rust

### Installing

```sh
cargo build --release
cp target/release/syndicator <where you store biniaries>
```

The config file is stored at `$XDG_CONFIG_HOME/syndicator/syndicator.toml`. Copy the
example config to the appropriate location and modify it to your needs.

Example:

```toml
feeds = [
    { url = "http://blog.qt.io/feed" , proxy = "socks5://some.ip:port"},
    { url = "https://www.digikam.org/index.xml", title = "custom title" },
    { url = "https://dbeaver.io/feed/" }
]

[database]
# url defaults to "$XDG_DATA_HOME/syndicator/feeds.db" if omitted
# Please use absolute paths since shell expansions and environment variables do
# not work right now.
url = "./feeds.db"
```

## Usage

You will see two panes after executing the binary (and hopefully some urls on
the left one if you have some feeds added to your config). The left one displays
your feeds and the right one the articles (empty by default, because no feed is
selected).

### Keybindings

Navigation is done by up and down arrow keys.

|       |                                                                                      |
| ----- | ------------------------------------------------------------------------------------ |
| r     | load feed                                                                            |
| R     | load all feeds                                                                       |
| enter | select item (opens a lift of articles on a feed and opens the article on an article) |
| q     | quit syndicator, or go back to the previous view                                     |
| d     | delete subscription                                                                  |
| e     | exports all feeds as opml format to the user download forlder                        |
| l     | view log                                                                             |
