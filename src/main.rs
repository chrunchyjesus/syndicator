#![forbid(unsafe_code)]
#![warn(clippy::all)]

#[macro_use]
extern crate diesel_migrations;

#[macro_use]
extern crate diesel;

mod db;
mod models;
mod network;
mod opml;
mod schema;
mod settings;
mod ui;
mod util;

use crossbeam_channel::Sender;
use db::DBCommand;
use models::sql::Subscription;
use settings::FeedItem;
use std::convert::TryInto;
use ui::UserData;
use util::request;

fn main() {
    cursive::logger::init();
    let settings: settings::Settings = confy::load("syndicator").expect("Could not load settings");

    let (db_tx, db_rx) = crossbeam_channel::unbounded();
    let (network_tx, network_rx) = crossbeam_channel::unbounded();

    let network_handle = network::init(db_tx.clone(), network_rx);
    let db_handle = db::init(settings.database.url, db_rx);

    sync_urls_with_disk(settings.feeds, &db_tx).expect("Could not sync with disk");

    ui::init(UserData { db_tx, network_tx });

    db_handle.join().expect("Could not join the db thread");
    network_handle
        .join()
        .expect("Could not join the network thread");
}

fn sync_urls_with_disk(feeds: Vec<FeedItem>, db_tx: &Sender<DBCommand>) -> Result<usize, String> {
    let subs = feeds
        .into_iter()
        .enumerate()
        .map(|(index, feed)| Subscription {
            url: feed.url,
            order: index.try_into().unwrap_or(i32::MAX),
            proxy: feed.proxy,
            title: feed.title,
        })
        .collect();

    request(&db_tx, |tx| DBCommand::InitSubscriptions(tx, subs)).map_err(|e| e.to_string())?
}
