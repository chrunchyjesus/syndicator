use serde::Deserialize;
use serde::Serialize;

#[derive(Serialize, Deserialize)]
pub struct Database {
    pub url: String,
}

impl Default for Database {
    fn default() -> Self {
        let project_dirs =
            directories_next::ProjectDirs::from("rs", "", "syndicator").expect("bad config dir");

        let mut db_path = project_dirs.data_dir().to_path_buf();
        db_path.push("feeds.db");
        Self {
            url: db_path.to_string_lossy().to_string(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FeedItem {
    pub url: String,
    pub title: Option<String>,
    pub proxy: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Settings {
    #[serde(default = "Database::default")]
    pub database: Database,
    pub feeds: Vec<FeedItem>,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            database: Default::default(),
            feeds: vec![],
        }
    }
}
