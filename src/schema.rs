table! {
    subscriptions (url) {
        url -> Text,
        #[sql_name="subscription_order"]
        order -> Integer,
        proxy -> Nullable<Text>,
        title -> Nullable<Text>,
    }
}

table! {
    subscriptions_data (url) {
        url -> Text,
        title -> Nullable<Text>,
        description -> Nullable<Text>,
    }
}

table! {
    article_count_cache (url) {
        url -> Text,
        total -> Integer,
        unread -> Integer,
    }
}

table! {
    articles (id) {
        id -> Text,
        title -> Text,
        url -> Text,
        author -> Text,
        content -> Text,
        #[sql_name="article_date"]
        date -> Text,
        is_read -> Bool,
        subscription_data_id -> Text,
    }
}

allow_tables_to_appear_in_same_query!(subscriptions, subscriptions_data, article_count_cache);
