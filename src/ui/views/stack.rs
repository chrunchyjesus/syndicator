use cursive::{
    traits::Nameable,
    views::{OnEventView, StackView, ViewRef},
    Cursive,
};

const STACK_VIEW_ID: &str = "stack";

pub fn init(siv: &mut Cursive) {
    let view = StackView::new().with_name(STACK_VIEW_ID);
    let view = OnEventView::new(view).on_event('q', |s| match find_stack(s) {
        Some(stack) if stack.len() == 1 => s.quit(),
        Some(mut stack) => {
            stack.pop_layer();
        }
        None => {}
    });
    siv.add_layer(view);
}

pub fn find_stack(siv: &mut Cursive) -> Option<ViewRef<StackView>> {
    siv.find_name(STACK_VIEW_ID)
}
