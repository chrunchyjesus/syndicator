use super::{stack::find_stack, VIEW_SIZE};
use crate::{
    db::DBCommand, models::Subscription, network::NetworkCommand, settings::FeedItem, util::request,
};
use crossbeam_channel::Sender;
use cursive::{
    traits::{Boxable, Nameable},
    views::{OnEventView, Panel, ScrollView, SelectView, ViewRef},
    Cursive,
};

type List = SelectView<Subscription>;

const SUBSCRIPTION_LIST_VIEW_ID: &str = "subscription_list";
const SUBSCRIPTION_LIST_SCROLL_VIEW_ID: &str = "subscription_list_scroll";

pub fn init(siv: &mut Cursive, db_tx: Sender<DBCommand>, network_tx: Sender<NetworkCommand>) {
    let view = SelectView::<Subscription>::new()
        .on_submit(on_submit(db_tx.clone()))
        .with_name(SUBSCRIPTION_LIST_VIEW_ID)
        .fixed_size(VIEW_SIZE);

    let view = OnEventView::new(view)
        .on_event('R', on_reload_all(network_tx.clone()))
        .on_event('r', on_reload(network_tx))
        .on_event('d', on_delete(db_tx.clone()))
        .on_event('g', jump_to_start)
        .on_event('G', jump_to_end);

    let view = ScrollView::new(view).with_name(SUBSCRIPTION_LIST_SCROLL_VIEW_ID);
    let view = Panel::new(view);

    if let Some(mut stack) = find_stack(siv) {
        stack.add_layer(view);
    }

    reload_subscriptions(db_tx)(siv)
}

fn on_submit(db_tx: Sender<DBCommand>) -> impl Fn(&mut Cursive, &Subscription) {
    move |siv: &mut Cursive, item: &Subscription| {
        super::article_list::init(siv, &item.url, db_tx.clone());
    }
}

fn on_reload_all(network_tx: Sender<NetworkCommand>) -> impl Fn(&mut Cursive) {
    on_subscription_list_with_sink(move |sink, view| {
        let items = view
            .iter()
            .map(|(_, sub)| sub)
            .map(|sub| FeedItem {
                url: sub.url.clone(),
                proxy: sub.proxy.clone(),
                title: sub.title.clone(),
            })
            .collect();
        let _ = network_tx.send(NetworkCommand::FetchSubscriptions(sink, items));
    })
}

fn on_reload(network_tx: Sender<NetworkCommand>) -> impl Fn(&mut Cursive) {
    on_subscription_list_with_sink(move |sink, view| {
        let items = view
            .selection()
            .map(|sub| FeedItem {
                url: sub.url.clone(),
                proxy: sub.proxy.clone(),
                title: sub.title.clone(),
            })
            .map(|item| vec![item])
            .unwrap_or_else(Vec::new);
        let _ = network_tx.send(NetworkCommand::FetchSubscriptions(sink, items));
    })
}

fn on_delete(db_tx: Sender<DBCommand>) -> impl Fn(&mut Cursive) {
    move |s| {
        let db_tx = db_tx.clone();
        let db_tx1 = db_tx.clone();

        let sx = find_subscription_list(s);
        if let Some(view) = sx {
            let url = match view.selection() {
                Some(item) => item.url.clone(),
                None => return,
            };
            let result = request(&db_tx, |tx| DBCommand::DeleteSubscription(tx, url));
            if let Ok(Err(err)) = result {
                log::error!("Cannot delete subscription: {}", err);
            }
        }
        reload_subscriptions(db_tx1)(s);
    }
}

fn jump_to_start(s: &mut Cursive) {
    if let Some(mut view) = find_subscription_list(s) {
        view.set_selection(0);
    }
    if let Some(mut view) = find_subscription_list_scroll(s) {
        view.scroll_to_important_area();
    }
}

fn jump_to_end(s: &mut Cursive) {
    if let Some(mut view) = find_subscription_list(s) {
        let i = view.len();

        view.set_selection(i - 1);
    }
    if let Some(mut view) = find_subscription_list_scroll(s) {
        view.scroll_to_important_area();
    }
}

fn find_subscription_list(s: &mut Cursive) -> Option<ViewRef<List>> {
    s.find_name(SUBSCRIPTION_LIST_VIEW_ID)
}

fn find_subscription_list_scroll(s: &mut Cursive) -> Option<ViewRef<ScrollView<List>>> {
    s.find_name(SUBSCRIPTION_LIST_SCROLL_VIEW_ID)
}

type Sink = crossbeam_channel::Sender<Box<dyn FnOnce(&mut Cursive) + 'static + Send>>;
fn on_subscription_list_with_sink<F>(func: F) -> impl Fn(&mut Cursive)
where
    F: FnOnce(Sink, &mut List) + Clone,
{
    move |s| {
        let mut view = match find_subscription_list(s) {
            Some(v) => v,
            None => return,
        };

        let sink = s.cb_sink().clone();
        let func = func.clone();

        func(sink, &mut *view);
    }
}

pub fn reload_subscriptions(db_tx: Sender<DBCommand>) -> impl Fn(&mut Cursive) {
    move |s| {
        let mut view = match find_subscription_list(s) {
            Some(v) => v,
            None => return,
        };

        let result = request(&db_tx, DBCommand::FindAllSubscriptions);

        let subs = match result {
            Ok(Ok(subs)) => subs,
            _ => return,
        };

        let selection = view.selection();
        view.clear();
        view.add_all(subscriptions_to_items(subs));

        if let Some(selection) = selection {
            let i = view
                .iter()
                .map(|item| item.1)
                .position(|item| item.url == selection.url);

            if let Some(i) = i {
                view.set_selection(i);
            }
        }
    }
}

fn subscriptions_to_items(
    subs: Vec<Subscription>,
) -> impl IntoIterator<Item = (String, Subscription)> {
    let total_indent = subs
        .iter()
        .map(|sub| crate::util::log10(sub.total as _) + 1)
        .max()
        .unwrap_or(0);
    let unread_indent = subs
        .iter()
        .map(|sub| crate::util::log10(sub.unread as _) + 1)
        .max()
        .unwrap_or(0);

    subs.into_iter().map(move |i| {
        let text = match &i.title {
            None => &i.url,
            Some(v) if v == &String::new() => &i.url,
            Some(v) => v,
        };

        let text = format!(
            "({unread:>unread_indent$}|{total:>total_indent$}) {text}",
            unread = i.unread,
            total = i.total,
            text = text,
            total_indent = total_indent,
            unread_indent = unread_indent
        );
        (text, i)
    })
}
