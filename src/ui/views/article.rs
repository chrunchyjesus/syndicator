use super::{stack::find_stack, VIEW_SIZE};
use crate::{models::Article, ui::parser::parse};
use cursive::{
    traits::Boxable,
    views::{LinearLayout, Panel, ScrollView, TextView},
    Cursive, View,
};

pub fn init(siv: &mut Cursive, article: &Article) {
    let layout = LinearLayout::vertical()
        .child(create_head(article))
        .child(create_content(article));

    if let Some(mut stack) = find_stack(siv) {
        stack.add_layer(layout);
    }
}

fn create_head(article: &Article) -> impl View {
    let text = match &article.author {
        t if t == &String::new() => String::new(),
        _ => format!("by {}", article.author),
    };
    let text = match &article.date {
        t if t == &String::new() => text,
        _ => format!("{}\non {}", text, article.date),
    };
    let text = match &article.url {
        t if t == &String::new() => text,
        _ => format!("{}\n{}", text, article.url),
    };
    let view = TextView::new(text);
    let title = TextView::new(&article.title);

    let layout = LinearLayout::vertical().child(title).child(view);

    Panel::new(layout)
}

fn create_content(article: &Article) -> impl View {
    let parse_result = parse(article);

    let view = TextView::new(parse_result).fixed_size(VIEW_SIZE);
    let view = ScrollView::new(view);

    Panel::new(view)
}
