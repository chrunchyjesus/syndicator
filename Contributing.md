# Contributing

## Setup

### Compartmentalisation

Install vagrant and execute `vagrant up` in root folder of this project. Every
time you want to test drive build binary execute `cargo build`, `vagrant ssh`
into the vagrant vm and `syndicator`. This has the benefit of not poluting your
machine.

## Code Style

The code style of this project follows `rustfmt`, which meanst running `rustfmt`
on the project automatically formats the code correctly.

To prevent inproperly formatted code being committed you can set up your editor
to run `cargo fmt` on save or use the following pre-commit hook for git hook to
check for conforance.

```shell
#!/usr/bin/env bash

# fail the whole script on any failures or using unset var
set -eu -o pipefail

# do not commit when the formatting is messy
cargo fmt -- --check
```

